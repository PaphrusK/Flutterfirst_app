import 'package:flutter/material.dart';
import 'body.dart';

void main() {
  runApp(HelloWorld());
}

class HelloWorld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
          title: Text(
            ("CS DAY CLASS"),
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Color(0xfff16f05)),
          ),
          backgroundColor: Colors.lime),
      body: Body(),
      backgroundColor: Color(0x64ede4ed),
    ));
  }
}
